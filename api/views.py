# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response
import rest_framework.status as HTTPstatus

from api.functions import complete_url, get_or_create_url, \
    get_og_url_from_short_id, list_of_urls


@api_view(['POST'])
def short_url(request):
    url = request.data.get('url', '')
    new_url = json.loads(request.data.get('new_url', 'false'))
    if not bool(url.strip()):
        response = dict(
            status=settings.ERROR_STATUS,
            data='The given URL is empty'
        )
        return Response(response, status=HTTPstatus.HTTP_400_BAD_REQUEST)
    url = complete_url(url)
    shortened_url, created = get_or_create_url(url, new_url=new_url)

    response_obj = dict(
        short_url=shortened_url
    )
    response = dict(
        status=settings.OK_STATUS,
        data=response_obj
    )
    if created:
        status = HTTPstatus.HTTP_201_CREATED
    else:
        status = HTTPstatus.HTTP_200_OK
    return Response(response, status=status)


@api_view(['GET'])
def list_urls(request):
    urls = list_of_urls()
    response = dict(
        status=settings.OK_STATUS,
        data=urls
    )
    return Response(response, status=HTTPstatus.HTTP_200_OK)


@api_view(['GET'])
def unshort(request, short_id):
    found, original_url = get_og_url_from_short_id(short_id)
    if not found:
        response = dict(
            status=settings.OK_STATUS,
            data='URL not found'
        )
        return Response(response, status=HTTPstatus.HTTP_404_NOT_FOUND)
    response = dict(
        status=settings.OK_STATUS,
        data=dict(
            long_url=original_url
        )
    )
    return Response(response, status=HTTPstatus.HTTP_200_OK)
