# -*- coding: utf-8 -*-
from django.conf import settings

from short_url.models import UrlDir
from short_url.short import ShortURL


def complete_url(url):
    """ takes a URL, and checks if it has protocol, if not, prepends http://
    
    :param url: the url to complete
    :type url: str
    :return: the url with protocol
    :rtype: str
    """
    default_protocol = 'http://'
    allowed_protocols = (
        'https://',
        'http://',
    )
    for protocol in allowed_protocols:
        if protocol in url:
            break
    else:
        url = default_protocol + url
    return url


def get_or_create_url(url, new_url=False, no_js=False):
    """ Gets or creates a new URL and returns a shortened one
    if new_url is set to True, a new record will be created in DB
    
    :param no_js: returns a different base if the client does an http 
    request and not an api call
    :type no_js: bool
    :param url: the long url
    :type url: str
    :param new_url: if we want to force the creatation of a new record 
    :type new_url: bool
    :return: the short url and a boolean depicting if a new resource was 
    created or not
    :rtype: tuple
    """
    base_url = '{domain}/{short}'
    no_js_url = '{domain}/n/{short}'
    if new_url:
        url_obj = UrlDir(original_url=url)
        url_obj.save()
        created = True
    else:
        # retrieve it
        url_obj = UrlDir.objects.filter(original_url=url).first()
        created = False
        if url_obj is None:
            # Save it (new record in db)
            url_obj = UrlDir(original_url=url)
            url_obj.save()
            created = True
    shortener = ShortURL()
    base = base_url if not no_js else no_js_url
    shortened_url = base.format(**dict(
        domain=settings.DOMAIN,
        short=shortener.encode(url_obj.pk)
    ))
    return shortened_url, created


def get_og_url_from_short_id(short_id):
    """ Search in the database for a record with the decoded short_id
    
    :param short_id: the encoded id
    :type short_id: str
    :return: a truple depicting if a resource was found in the database and 
    the original url if it was found. Flase and empty if not
    :rtype: tuple 
    """
    short_url = ''
    shortener = ShortURL()
    try:
        url_id = shortener.decode(short_id)
    except ValueError:
        found = False
        return found, short_url
    try:
        unshort_url = UrlDir.objects.get(pk=url_id)
        found = True
    except UrlDir.DoesNotExist:
        found = False
        return found, short_url
    unshort_url.hit_count += 1
    unshort_url.save()
    return found, unshort_url.original_url


def list_of_urls(no_js=False):
    """ Get a list of all the urls in DB, and append a new key with the short id
    
    :param no_js: True if we need a short url that works without js
    :type no_js: bool
    :return: list of dictionaries with keys: ['pk', 'original_url', 
        'hit_count', 'short_url']
    :rtype: list
    """
    base_url = '{domain}/{short_url}'
    no_js_url = '{domain}/n/{short_url}'
    base = base_url if not no_js else no_js_url

    urls = UrlDir.objects.all().values(
        'pk',
        'original_url',
        'hit_count'
    )
    urls = list(urls)

    shortener = ShortURL()
    for url in urls:
        url.update(
            dict(short_url=base.format(
                **dict(
                    domain=settings.DOMAIN,
                    short_url=shortener.encode(url['pk'])
                )),
                original_url=complete_url(url['original_url'])
            )
        )
    return urls
