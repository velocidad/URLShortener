# -*- coding: utf-8 -*-
from django.conf.urls import url

from api.views import short_url, list_urls, unshort

urlpatterns = [
    url(r'^short_this/$', short_url, name='short_url'),
    url(r'^list/$', list_urls, name='list_urls'),
    url(r'^unshort/(?P<short_id>.+)/$', unshort, name='unshort')
]