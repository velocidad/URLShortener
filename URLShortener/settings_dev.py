# -*- coding: utf-8 -*-
from .settings import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'short_db',
        'USER': 'root',
        'PASSWORD': 'A8d32e08.',
        'ATOMIC_REQUESTS': True
    }
}

INSTALLED_APPS += [
    'django_extensions',
    'werkzeug'
]

STATIC_ROOT = ''

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'templates/static/'),
)

DOMAIN_PLAIN = 'localhost:8000'
DOMAIN = 'http://' + DOMAIN_PLAIN