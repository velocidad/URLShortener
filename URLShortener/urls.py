from django.conf.urls import url, include
from django.contrib import admin

from short_url.views import home, list_urls, nojs_home, take_and_redirect, \
    url_list

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('api.urls')),
    url(r'^list/$', list_urls),
    url(r'^njs/$', nojs_home, name='no_js_home'),

    url(r'^n/(?P<short_id>.+)/', take_and_redirect,
        name='take_and_redirect'),
    url(r'^url_list/$', url_list, name='url_list'),
    url(r'^$', home),

    url(r'^(?P<short_id>.+)/$', home)
]

