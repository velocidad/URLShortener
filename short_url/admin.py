# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from short_url.models import UrlDir

admin.site.register(UrlDir)
