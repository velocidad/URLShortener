# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from six import python_2_unicode_compatible


@python_2_unicode_compatible
class UrlDir(models.Model):
    original_url = models.URLField(verbose_name='URL')
    created_at = models.DateTimeField(auto_now_add=True)
    custom_url = models.CharField(max_length=16,
                                  verbose_name='Custom URL',
                                  blank=True, default='')
    hit_count = models.PositiveIntegerField(verbose_name='Hits',
                                            default=0)

    def __str__(self):
        return self.original_url