# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.shortcuts import render, redirect

from api.functions import complete_url, get_or_create_url, \
    get_og_url_from_short_id, list_of_urls


def home(request, short_id=None):
    return render(request=request, template_name='index.html', context={})


def list_urls(request):
    return render(request=request,
                  template_name='index.html',
                  context={'list':True})


def nojs_home(request, not_found=None):
    context = dict()
    if not_found:
        context['error'] = 'The given URL does not exists'
    if request.method == 'POST':
        # validate url
        validate = URLValidator()
        url = request.POST.get('url-input', '')
        new_url = request.POST.get('generate-new', False)
        if not bool(url.strip()):
            context['error'] = 'The given URL is empty'
        url = complete_url(url)
        valid = True
        try:
            validate(url)
        except ValidationError, e:
            print e
            context['error'] = 'The given URL is not valid'
            valid = False
        # if valid encode
        if valid:
            shortened_url, created = get_or_create_url(
                url, new_url=new_url, no_js=True)
            context['success'] = shortened_url
    print context
    return render(request=request, template_name='nojs_index.html', context=context)


def take_and_redirect(request, short_id):
    found, original_url = get_og_url_from_short_id(short_id)
    if not found:
        return render(
            request=request, template_name='nojs_index.html',
            context={'error': 'URL not found'})
    return redirect(original_url)


def url_list(request):
    urls = list_of_urls(no_js=True)
    return render(request=request, template_name='nojs_list.html',
                  context=dict(urls=urls))